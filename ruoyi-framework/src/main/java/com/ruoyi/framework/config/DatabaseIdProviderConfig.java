package com.ruoyi.framework.config;

import java.util.Properties;

import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseIdProviderConfig {
	
    @Bean(name = "databaseIdProvider")
    public DatabaseIdProvider databaseIdProvider(){
        DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
        Properties p = new Properties();
        p.setProperty("Oracle", "oracle");
        p.setProperty("MySQL", "mysql");
        p.setProperty("PostgreSQL", "pgsql");
        p.setProperty("DB2", "db2");
        p.setProperty("SQL Server", "mssql");
        databaseIdProvider.setProperties(p);
        return databaseIdProvider;
    }

}
